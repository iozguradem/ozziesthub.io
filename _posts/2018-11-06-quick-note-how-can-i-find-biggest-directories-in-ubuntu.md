---
layout: post
title:  "#12 How Can I Find Biggest Directories In Ubuntu"
date:   2018-11-06 21:56
categories: [English, Notes]
tags: ubuntu, directory, size, biggest, folder
meta: ubuntu, directory, size, biggest, folder
author: ozziest
---

<a href="https://pixabay.com/en/books-pages-story-stories-notes-1245690/" target="_blank">
    <img src="/images/posts/notes.jpg" class="center" />
</a>

> In case I forget later, this is a simple note for me.

### Introduction

When the problem is how can you find the folder which is the biggest on ubuntu file system, you can use simple command.

### Example

```bash
$ du -sch /*
```