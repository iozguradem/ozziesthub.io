Özgür Adem Işıklı
============
Email: i.ozguradem@gmail.com 
Tel: +90 554 632 26 93
Web: http://ozguradem.net

I have been working as software developer since 2010, especially in web development. I have created different web applications with using C#, PHP and NodeJS. I am familiar modern web frameworks (back-end or front-end) such as .Net MVC, .Net Core, Laravel, Symfony, AngularJS, VueJS or React. I am also familiar unit testing concept and libraries such as xUnit, PHPUnit, CodeCeption, Mockery, Mocha or Jasmine. Further I would be careful about SOLID principles and best practices when I develop a web application in order to write readable, maintainable and testable code. And that's why i love refactoring... In addition, I love to play with Symfony components as a hobby. Also I have experience about continuous integration tools such as Gitlab-CI, Travis-CI and Jenkins. You may check my GitHub account and review my codes which I've written if you want to learn more information about me.

 The aim which in my career is developing maintainable high quality codes. For this purpose I am improving myself  about unit and integration testing. Also I am a big fan of CI/CD processes.

## SKILLS

  - Languages: C# NodeJS Javascript PHP 
  - Back-End Frameworks: .Net MVC .Net Core Laravel Symfony2 CodeIgniter ExpressJS AdonisJS 
  - Front-End Frameworks & Libraries: AngularJS VueJS React 
  - Testing Libraries: xUnit PHPUnit Mockery PHPSpec CodeCeption Jasmine Mocha NUnit 
  - Databases: MySQL MSSQL Oracle MongoDB 
  - Others: Git Linux Systems Memcache Redis RabbitMQ ROS Jenkins TravisCI Docker 

## EMPLOYMENT

### *Software Developer*, [Polisoft](http://polisoft.co.uk) (2015-07 — Present)

I have been developed a single page web application about insurance industry with using C# and AngularJS. The application have connection with a lot of SOAP web services in order to operate all system. 

Also I am lead developer for a project which users can design a complex system. The application allows to user to create another web application in constant rules. Users can design tables, wizards or forms. They can set the rules of forms and they can write even formulas like Microsoft Excel. Additionally, users can create Rest APIs in order to system can be used by third by application.

### *Software Consultant*, Freelance (2015-01 — 2015-06)

I helped to create a humonoid robot which is developed by RA Nuretting Gokhan Adar, Sakarya University. We used C++ as a language and Roboting Operating System (ROS) in that project. All equations and maths have been designed by Mr. Adar. I have just helped to implementations.

### *Software Developer*, [Ahir Inc](http://ahir.com.tr) (2013-02 — 2016-07)

I have created three single page applications which can run on cloud with using PHP and Angular. I have used CodeIgniter, Laravel and Symfony 2 as back end framework. In one application, jQuery has been used. In other applications, AngularJS has been used as front end framework.

### *Software Developer*, [Birey](http://birey.com.tr) (2012-05 — 2013-02)

I have developed the web application which is already exists and is about school management system. I was using old school  ways bacause of the project. That's why I quite. Nevertheless I have added new modules on the application.




## EDUCATION

### Sakarya University (2017-07 — Present)



### Sakarya University (2006-09 — 2010-06)



### A.N. Bilimli Technical High School (2002-09 — 2006-05)






## WRITING

### [Package Development With PHP (Turkish)](https://leanpub.com/phpilepaketgelistirme) (2015-01)

The book is about how can you develop and test a package which is readable, maintainable in PHP.



## SERVICE

### *Founder*, [Mavidurak IO](http://mavidurak.github.io/) (2014-08 — Present)

Mavidurak-IO is a software community which is located in Sakarya, near of the Istanbul. We had set a lot of meetings and training with developers who live in Sakarya.

### *Volunteer, Trainer*, [Sakarya Coders](https://www.meetup.com/Sakarya-Coders/) (2017-04 — Present)

Yet another software community in Sakarya.





