---
layout: page
title: About
---

My name is Özgür Adem Işıklı.

I work as the Lead Developer for the Web Development at <a href="https://www.polisoft.co.uk/" target="_blank">Polisoft</a>. I'm located in Sakarya, Turkey.

I'm a Web Developer since 2012 and I write and talk about .Net, JavaScript, Clean Code, Refactoring and DevOps. 

I am a huge fan of discovering. I have a long list full of with big dreams. I am interested as amateur with music, visual arts and literature.

Please feel free to contact with me about any topic. I'll answer your questions as soon as possible. My Twitter id is <a href="http://twitter.com/iozguradem" target="_blank">@iozguradem</a>.

<div>
    <a class="github" target="_blank" href="http://github.com/ozziest"></a>
    <a class="twitter" target="_blank" href="http://twitter.com/iozguradem"></a>
    <a class="linkedin" target="_blank" href="http://tr.linkedin.com/in/ozguradem/"></a>
</div>

<br /><br />